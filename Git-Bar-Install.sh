#!/bin/bash

git clone --bare https://gitlab.com/Otiker/xfce-1 $HOME/.dotfiles

function config {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}
config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

#mkdir -p .config-backup
config checkout

if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
#    dirList=$($config checkout 2>&1 | egrep '\s+\.' | awk '{print}' | awk '{print}' | sed -e 's/^[ \t]*//' | sed 's|\(.*\)/.*|\1|' | uniq)
#    for dir in $dirList; do
#      dirType=$(file $dir | awk '{print $2}')
#      if [[ $dirType == "directory" ]]; then
#        mkdir -p .config-backup/$dir
#      fi
#    done
    config checkout 2>&1 | egrep "\s+\." | awk '{print}' | sed -e 's/^[ \t]*//' | xargs -I{} rm -rf {}
fi;
config checkout
config config status.showUntrackedFiles no
